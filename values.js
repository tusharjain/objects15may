// Return all of the values of the object's own properties.
// Ignore functions
// http://underscorejs.org/#values


export function values(obj) {
    let valueArray = []; 
    for(let key in obj) {
        valueArray.push(obj[key]);
    }
    return valueArray; 
}