// Retrieve all the names of the object's properties.
// Return the keys as strings in an array.
// Based on http://underscorejs.org/#keys

export function keys(obj) {
    let keyArray = [];
    for(let key in obj) {
        keyArray.push(key); 
    }
    return keyArray; 
}