// Returns a copy of the object where the keys have become the values and the values the keys.
// Assume that all of the object's values will be unique and string serializable.
// http://underscorejs.org/#invert

export function invert(obj) {
    let invertObj = {}; 
    for(let key in obj) {
        invertObj[(obj[key]).toString()] = key.toString(); 
    }
    return invertObj; 
}