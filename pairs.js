// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs

export function pairs(obj) {
    let listArray = []; 
    for(let key in obj) {
        listArray.push([key, obj[key]]);
    }
    return listArray; 
}