// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
// http://underscorejs.org/#mapObject

export function mapObject(obj, cb) {
    let newObj = {};
    for(let key in obj) {
        newObj[key] = cb(obj[key], key); 
    }
    return newObj; 
}