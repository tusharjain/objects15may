import {invert} from '../invert.js'
import {keys} from '../keys.js'

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function test(func, input, expect) {
    let output = func(input); 
    console.log(output); 
    let check = 1; 
    for(let i in expect) {
        if(!keys(output).includes(i) || expect[i] !== output[i]) {
            check = 0; 
        }
    }
    if(check) {
        console.log('Test Passed');
    }
    else {
        console.log('Test Failed'); 
    }
}

test(invert, testObject, {'Bruce Wayne': 'name', '36': 'age', 'Gotham': 'location'});

