import {defaults} from '../defaults.js'
import {keys} from '../keys.js'

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function test(func, input, expect) {
    let output = func(input, {name: 'Sherlock Holmes', profession: 'detective'}); 
    console.log(output); 
    let check = 1; 
    for(let i in expect) {
        if(!keys(output).includes(i) || expect[i] !== output[i]) {
            check = 0; 
        }
    }
    if(check) {
        console.log('Test Passed');
    }
    else {
        console.log('Test Failed'); 
    }
}

test(defaults, testObject, { name: 'Bruce Wayne', age: 36, location: 'Gotham', profession: 'detective' });

