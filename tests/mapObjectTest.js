import {mapObject} from '../mapObject.js';

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function callback(val, key) {
    switch(key) {
        case 'name': return 'Dr.' + val;
        case 'age' : return val + 5; 
        case 'location': return 'Metropolis';
    }
}

function test(func, input, expect) {
    let output = func(input, callback); 
    
    let check = 1; 
    for(let i in expect) {
        if(expect[i] !== output[i]) {
            check = 0; 
            break; 
        }
    }
    if(check) {
        console.log('Test Passed');
    }
    else {
        console.log('Test Failed'); 
    }
}

test(mapObject, testObject, { name: 'Dr.Bruce Wayne', age: 41, location: 'Metropolis' });

