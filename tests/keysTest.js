import {keys} from '../keys.js'

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function test(func, input, expect) {
    let output = func(input); 

    let check = 1; 
    for(let i = 0; i < expect.length; i++) {
        if(output[i] !== expect[i]) {
            check = 0; 
            break; 
        }
    }
    if(check) {
        console.log('Test Passed');
    }
    else {
        console.log('Test Failed'); 
    }
}

test(keys, testObject, ['name', 'age', 'location']);

