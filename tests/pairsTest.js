import {pairs} from '../pairs.js'

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function test(func, input, expect) {
    let output = func(input); 
    let check = 1; 
    for(let i = 0; i < expect.length; i++) {
        if(expect[i][0] !== output[i][0] || expect[i][1] !== output[i][1]) {
            check = 0; 
        }
    }
    if(check) {
        console.log('Test Passed');
    }
    else {
        console.log('Test Failed'); 
    }
}

test(pairs, testObject, [['name', 'Bruce Wayne'], ['age', 36], ['location', 'Gotham']]);

